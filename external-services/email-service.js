const nodemailer = require("nodemailer");
require('dotenv/config');

const environmentConfigs = process.env;

exports.sendEmail = async (studentPayload, password) => {
    try {
        if (studentPayload) {
            let transporter = nodemailer.createTransport({
                service: "gmail",
                secure: true,
                auth: {
                    user: environmentConfigs.email,
                    pass: environmentConfigs.password,
                },
            });

            let info = await transporter.sendMail({
                from: environmentConfigs.email,
                to: studentPayload.userEmail,
                subject: `Student desk`,
                text: `Hi ${studentPayload.firstName}, \n We have created your account, please find below default credentials. \n User name : ${studentPayload.userEmail} \n password : ${password}`
            });

            console.log("Message sent: %s", info.messageId);
            console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
            return true;
        }
    } catch (error) {
        console.log(error);
    }
}

