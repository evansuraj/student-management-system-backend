const { v4: uuidv4 } = require('uuid');
const { body } = require('express-validator')
const { validationResult } = require('express-validator');
const permissionManager = require('../../business/permission/permission-manager')


exports.validate = (method) => {
    switch (method) {
        case 'savePermission': {
            return [
                body('permissionCode', 'permission code is required.').exists().notEmpty(),
                body('permissionName', 'permission name is required.').exists().notEmpty(),
            ]
        }
        case 'updatePermission':
            return [
                body('permissionCode', 'permission code is required.').exists().notEmpty(),
                body('permissionName', 'permission name is required.').exists().notEmpty(),
            ]
    }
}

exports.savePermission = async (req, res) => {
    try {
        const errors = validationResult(req)
        if (errors.isEmpty()) {
            const payload = req.body
            if (payload) {
                const permission = {
                    permissionId: uuidv4(),
                    permissionCode: payload.permissionCode,
                    permissionName: payload.permissionName,
                    permissionDescription: payload.permissionDescription,
                    isActive: true
                }

                const savedResult = await permissionManager.savePermission(permission)
                if (savedResult.validity) {
                    res.status(201).json(savedResult)
                } else {
                    res.status(500).json({ error: savedResult.error, success: false })
                }
            } else {
                res.status(400).json({ error: "Invalid model", success: false })
            }
        } else {
            res.status(422).json({ errors: errors.array() })
            return
        }
    } catch (error) {
        res.status(500).json({ error: error, success: false })
    }
}

exports.getPermissionDetails = async (req, res) => {
    try {
        const permissionDetails = await permissionManager.getPermissionDetails()
        if (permissionDetails) {
            res.status(200).json(permissionDetails)
        } else {
            res.status(204).json()
        }
    } catch (error) {
        res.status(500).json({ error: error, success: false })
    }
}

exports.getPermissionDetail = async (req, res) => {
    try {
        const permissionId = req.params.permissionId
        if (permissionId) {
            const permission = await permissionManager.getPermission(permissionId)
            if (permission) {
                res.status(200).json(permission)
            } else {
                res.status(204).json()
            }
        } else {
            res.status(500).json({ error: "Permission id is required.", success: false })
        }
    } catch (error) {
        res.status(500).json({ error: error, success: false })
    }
}

exports.getFilteredPermission = async (req, res) => {
    try {
        const filteredParams = req.body.filterParams
        if (filteredParams) {
            const permissionDetails = await permissionManager.filterPermission(filteredParams)
            if (permissionDetails) {
                res.status(200).json(permissionDetails)
            } else {
                res.status(204).json()
            }
        } else {
            res.status(400).json({ error: "filteredParams are required.", success: false })
        }
    } catch (error) {
        res.status(500).json({ error: error, success: false })
    }
}

exports.updatePermission = async (req, res) => {
    try {
        const errors = validationResult(req)
        if (errors.isEmpty()) {
            const permission = {
                permissionId: req.body.permissionId,
                permissionCode: req.body.permissionCode,
                permissionName: req.body.permissionName,
                permissionDescription: req.bodypermissionDescription,
                isActive: true
            }

            const updatedResult = await permissionManager.updatePermission(permission)
            if (updatedResult) {
                res.status(201).json(updatedResult)
            } else {
                res.status(500).json({ error: 'Failed to update.', success: false })
            }
        } else {
            res.status(422).json({ errors: errors.array() })
            return
        }
    } catch (error) {
        res.status(500).json({ error: error, success: false })
    }
}

exports.deletePermission = async (req, res) => {
    try {
        const permissionId = req.params.permissionId
        const isRemove = req.params.isRemove
        if (permissionId) {
            const deleted = await permissionManager.deletePermission(permissionId, isRemove)
            if (deleted) {
                res.status(200).json(deleted)
            } else {
                res.status(204).json()
            }
        } else {
            res.status(400).json({ error: 'Failed to delete.', success: false })
        }
    } catch (error) {
        res.status(500).json({ error: error, success: false })
    }
}