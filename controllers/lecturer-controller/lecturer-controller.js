
const { v4: uuidv4 } = require('uuid');
const { body } = require('express-validator')
const { validationResult } = require('express-validator');
const lecturerManager = require('../../business/lecturer/lecturer-manager')

exports.validate = (method) => {
    switch (method) {
        case 'saveLecturer': {
            return [
                body('lecturerFirstName', 'lecturer first name is required.').exists().notEmpty(),
                body('lecturerLastName', 'lecturer last name is required.').exists().notEmpty(),
                body('email', 'email is required.').exists().notEmpty(),
                body('email', 'invalid email.').isEmail(),
                body('nic', 'nic is required.').exists().notEmpty()
            ]
        }
        case 'updateLecturer':
            return [
                body('lecturerFirstName', 'lecturer first name is required.').exists().notEmpty(),
                body('lecturerLastName', 'lecturer last name is required.').exists().notEmpty(),
                body('email', 'email is required.').exists().notEmpty(),
                body('email', 'invalid email.').isEmail(),
                body('nic', 'nic is required.').exists().notEmpty()
            ]
    }
}


exports.saveLecturer = async (req, res) => {
    try {
        const errors = validationResult(req)
        if (errors.isEmpty()) {
            const payload = req.body
            if (payload) {
                const lecturer = {
                    lecturerId: uuidv4(),
                    lecturerFirstName: payload.lecturerFirstName,
                    lecturerLastName: payload.lecturerLastName,
                    lecturerMiddleName: payload.lecturerMiddleName,
                    email: payload.email,
                    lecturerOtherEmail: payload.lecturerOtherEmail,
                    nic: payload.nic,
                    passsportId: payload.passsportId,
                    profileImage: payload.profileImage,
                    lecturerAttachments: payload.lecturerAttachments,
                    isActive: true
                }

                const lecturerResult = await lecturerManager.saveLecturer(lecturer)
                if (lecturerResult && lecturerResult.validity) {
                    res.status(201).json(lecturerResult)
                } else {
                    res.status(500).json({ error: lecturerResult.error, success: false })
                }
            } else {
                res.status(400).json({ error: "Invalid model", success: false })
            }
        } else {
            res.status(422).json({ errors: errors.array() })
            return
        }
    } catch (error) {
        res.status(500).json({ error: error, success: false })
    }
}

exports.getLecturerDetails = async (req, res) => {
    try {
        const lecturerDetails = await lecturerManager.getLecturerDetails()
        if (lecturerDetails) {
            res.status(200).json(lecturerDetails)
        } else {
            res.status(204).json()
        }
    } catch (error) {
        res.status(500).json({ error: error, success: false })
    }
}

exports.getLecturer = async (req, res) => {
    try {
        const lecturerId = req.params.lecturerId
        if (lecturerId) {
            const lecturer = await lecturerManager.getLecturer(lecturerId)
            if (lecturer) {
                res.status(200).json(lecturer)
            } else {
                res.status(204).json()
            }
        }
    } catch (error) {
        res.status(500).json({ error: error, success: false })
    }
}

exports.updateLecturer = async (req, res) => {
    try {
        const error = validationResult(req)
        if (error.isEmpty()) {
            const payload = req.body
            if (payload) {
                const lecturer = {
                    lecturerId: payload.lecturerId,
                    lecturerFirstName: payload.lecturerFirstName,
                    lecturerLastName: payload.lecturerLastName,
                    lecturerMiddleName: payload.lecturerMiddleName,
                    email: payload.email,
                    lecturerOtherEmail: payload.lecturerOtherEmail,
                    nic: payload.nic,
                    passsportId: payload.passsportId,
                    profileImage: payload.profileImage,
                    lecturerAttachments: payload.lecturerAttachments,
                    isActive: payload.isActive,
                }

                const updatedResult = await lecturerManager.updateLecturer(lecturer)
                if (updatedResult) {
                    res.status(201).json(updatedResult)
                } else {
                    res.status(500).json({ error: e, success: false })
                }
            } else {
                res.status(400).json({ error: "Invalid model", success: false })
            }
        } else {
            res.status(422).json({ errors: errors.array() })
            return
        }
    } catch (error) {
        res.status(500).json({ error: error, success: false })
    }
}

exports.deleteLecturer = async (req, res) => {
    try {
        const lecturerId = req.params.lecturerId
        if (lecturerId) {
            const deleted = await lecturerManager.deleteLecturer(lecturerId)
            if (deleted) {
                res.status(200).json(deleted)
            } else {
                res.status(204).json()
            }
        } else {
            res.status(400).json({ error: "Lecturer id required.", success: false })
        }
    } catch (error) {
        res.status(500).json({ error: error, success: false })
    }
}
