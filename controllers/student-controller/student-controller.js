const { v4: uuidv4 } = require('uuid');
const { body } = require('express-validator')
const { validationResult } = require('express-validator');
const studentManager = require('../../business/student/student-manager')


exports.validate = (method) => {
    switch (method) {
        case 'saveStudent': {
            return [
                body('studentFirstName', 'user name is required.').exists().notEmpty(),
                body('studentLastName', 'user email is required.').exists().notEmpty(),
                body('email', 'user password is required.').exists().notEmpty(),
                body('nic', 'Nic is required').exists().notEmpty()
            ]
        }
        case 'updateStudent':
            return [
                body('studentFirstName', 'user name is required.').exists().notEmpty(),
                body('studentLastName', 'user email is required.').exists().notEmpty(),
                body('email', 'user password is required.').exists().notEmpty(),
                body('nic', 'Nic is required').exists().notEmpty()
            ]
    }
}


exports.saveStudent = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.status(422).json({ errors: errors.array() })
            return
        } else {
            const payload = req.body
            if (payload) {
                const student = {
                    studentId: uuidv4(),
                    studentFirstName: payload.studentFirstName,
                    studentLastName: payload.studentLastName,
                    studentMiddleName: payload.studentMiddleName,
                    email: payload.email,
                    studentOtherEmail: payload.studentOtherEmail,
                    nic: payload.nic,
                    passsportId: payload.passsportId,
                    profileImage: payload.profileImage,
                    studentAttachments: payload.studentAttachments,
                    isActive: true,
                    gender: payload.gender,
                    dob: payload.dob,
                    age: payload.age,
                    batchYear: payload.batchYear,
                    batch: payload.batch,
                    studentSystemNumber: payload.studentSystemNumber
                }

                const savedResult = await studentManager.saveStudent(student)
                if (savedResult && savedResult.validity) {
                    res.status(201).json(savedResult)
                } else {
                    res.status(500).json({ error: savedResult.error, success: false })
                }
            } else {
                res.status(400).json({ error: 'Invalid model', success: false })
            }
        }
    } catch (error) {
        res.status(500).json({ error: error, success: false })
    }
}

exports.getStudents = async (req, res) => {
    try {
        const students = await studentManager.getStudents()
        if (students && students.validity) {
            res.status(200).json(students)
        } else {
            res.status(204).json()
        }
    } catch (error) {
        res.status(500).json({ error: error, success: false })
    }
}

exports.getStudent = async (req, res) => {
    try {
        const studentId = req.params.studentId
        if (studentId) {
            const student = await studentManager.getStudent(studentId)
            if (student && student.validity) {
                res.status(200).json(student)
            } else {
                res.status(204).json()
            }
        } else {
            res.status(400).json({ error: 'Student id is required.', success: false })
        }
    } catch (error) {
        res.status(500).json({ error: error, success: false })
    }
}

exports.updateStudent = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.status(422).json({ errors: errors.array() })
            return
        } else {
            const payload = req.body
            if (payload) {
                const student = {
                    studentId: payload.studentId,
                    studentFirstName: payload.studentFirstName,
                    studentLastName: payload.studentLastName,
                    studentMiddleName: payload.studentMiddleName,
                    email: payload.email,
                    studentOtherEmail: payload.studentOtherEmail,
                    nic: payload.nic,
                    passsportId: payload.passsportId,
                    profileImage: payload.profileImage,
                    studentAttachments: payload.studentAttachments,
                    isActive: payload.isActive,
                    gender: payload.gender,
                    dob: payload.dob,
                    age: payload.age,
                    batchYear: payload.batchYear,
                    batch: payload.batch,
                    studentSystemNumber: payload.studentSystemNumber
                }

                const updatedResult = await studentManager.updateStudent(student)
                if (updatedResult) {
                    res.status(201).json(updatedResult)
                } else {
                    res.status(500).json({ error: 'Failed to update.', success: false })
                }
            } else {
                res.status(400).json({ error: 'Invalid model.', success: false })
            }
        }
    } catch (error) {
        res.status(500).json({ error: error, success: false })
    }
}

exports.deleteStudent = async (req, res) => {
    try {
        const studentId = req.params.studentId
        if (studentId) {
            const deleted = await studentManager.deleteStudent(studentId)
            if (deleted) {
                res.status(200).json(deleted)
            } else {
                res.status(204).json()
            }
        } else {
            res.status(400).json({ error: 'Student id is required.', success: false })
        }
    } catch (error) {
        res.status(500).json({ error: error, success: false })
    }
}
