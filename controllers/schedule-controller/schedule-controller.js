const { v4: uuidv4 } = require('uuid');
const { body } = require('express-validator')
const { validationResult } = require('express-validator');
const scheduleManager = require('../../business/schedule/schedule-manager')


exports.validate = (method) => {
    switch (method) {
        case 'saveSchedule': {
            return [
                body('scheduledDate', 'schedule date is required.').exists().notEmpty(),
                body('scheduledStartTime', 'schedule start time is required.').exists().notEmpty(),
                body('scheduledEndTime', 'schedule end time is required.').exists().notEmpty(),
                body('scheduledLocation', 'schedule location is required.').exists().notEmpty(),
                body('scheduledModule', 'schedule module is required').exists().notEmpty(),
                body('scheduledAttendingLecturer', 'attending lecture is required.').exists().notEmpty()
            ]
        }
        case 'updateSchedule':
            return [
                body('scheduledDate', 'schedule date is required.').exists().notEmpty(),
                body('scheduledStartTime', 'schedule start time is required.').exists().notEmpty(),
                body('scheduledEndTime', 'schedule end time is required.').exists().notEmpty(),
                body('scheduledLocation', 'schedule location is required.').exists().notEmpty(),
                body('scheduledModule', 'schedule module is required').exists().notEmpty(),
                body('scheduledAttendingLecturer', 'attending lecture is required.').exists().notEmpty()
            ]
    }
}



exports.saveSchedule = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (errors.isEmpty()) {
            const payload = req.body
            if (payload) {
                const schedule = {
                    scheduleId: uuidv4(),
                    scheduleType: payload.scheduleType,
                    batch: payload.batch,
                    scheduledDate: payload.scheduledDate,
                    scheduledStartTime: payload.scheduledStartTime,
                    scheduledEndTime: payload.scheduledEndTime,
                    scheduledLocation: payload.scheduledLocation,
                    scheduledModule: payload.scheduledModule,
                    scheduledAttendingLecturer: payload.scheduledAttendingLecturer,
                    selectedCourse: payload.selectedCourse,
                    isActive: true
                }

                const savedResult = await scheduleManager.saveSchedule(schedule)
                if (savedResult) {
                    res.status(201).json(savedResult)
                } else {
                    res.status(500).json({ error: 'Failed to save schedule', success: false })
                }
            } else {
                res.status(400).json({ error: 'Invalid model', success: false })
            }
        } else {
            res.status(422).json({ errors: errors.array() })
            return
        }
    } catch (error) {
        res.status(500).json({ error: error, success: false })
    }
}

exports.viewSchedules = async (req, res) => {
    try {
        const viewSchedules = await scheduleManager.viewAllSchedules()
        if (viewSchedules) {
            res.status(200).json(viewSchedules)
        } else {
            res.status(204).json()
        }
    } catch (error) {
        res.status(500).json({ error: error, success: false })
    }
}

exports.getAllSchedule = async (req, res) => {
    try {
        const scheduleDetails = await scheduleManager.getScheduleDetails()
        if (scheduleDetails) {
            res.status(200).json(scheduleDetails)
        } else {
            res.status(204).json()
        }
    } catch (error) {
        res.status(500).json({ error: error, success: false })
    }
}

exports.getSchduleDetail = async (req, res) => {
    try {
        const scheduleId = req.params.scheduleId
        if (scheduleId) {
            const schedule = await scheduleManager.getScheduleDetail(scheduleId)
            if (schedule) {
                res.status(200).json(schedule)
            } else {
                res.status(204).json()
            }
        } else {
            res.status(400).json({ error: 'schedule id is required.', success: false })
        }
    } catch (error) {
        res.status(500).json({ error: error, success: false })
    }
}

exports.getFilteredSchedule = async (req, res) => {
    try {
        const filteredParams = req.body.filterParams
        if (filteredParams) {
            const scheduleDetails = await scheduleManager.filterBySchduleType(filteredParams)
            if (scheduleDetails) {
                res.status(200).json(scheduleDetails)
            } else {
                res.status(204).json()
            }
        } else {
            res.status(400).json({ error: 'Filter params are required.', success: false })
        }
    } catch (error) {
        res.status(500).json({ error: error, success: false })
    }
}

exports.updateSchedule = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (errors.isEmpty()) {
            const payload = req.body
            if (payload) {
                const schedule = {
                    scheduleId: payload.scheduleId,
                    batch: payload.batch,
                    scheduleType: payload.scheduleType,
                    scheduledDate: payload.scheduledDate,
                    scheduledStartTime: payload.scheduledStartTime,
                    scheduledEndTime: payload.scheduledEndTime,
                    scheduledLocation: payload.scheduledLocation,
                    scheduledModule: payload.scheduledModule,
                    scheduledAttendingLecturer: payload.scheduledAttendingLecturer,
                    selectedCourse: payload.selectedCourse,
                    isActive: payload.isActive
                }

                const updatedResult = await scheduleManager.updateSchedule(schedule)
                if (updatedResult) {
                    res.status(201).json(updatedResult)
                } else {
                    res.status(500).json({ error: 'Failed to update.', success: false })
                }
            } else {
                res.status(400).json({ error: 'invalid model.', success: false })
            }
        } else {
            res.status(422).json({ errors: errors.array() })
            return
        }
    } catch (error) {
        res.status(500).json({ error: error, success: false })
    }
}

exports.deleteSchedule = async (req, res) => {
    try {
        const scheduleId = req.params.scheduleId
        const isRemove = req.params.isRemove
        if (scheduleId) {
            const deleted = await scheduleManager.deleteSchedule(scheduleId, isRemove)
            if (deleted) {
                res.status(200).json(deleted)
            } else {
                res.status(204).json()
            }
        } else {
            res.status(400).json({ error: 'schedule id is required.', success: false })
        }
    } catch (error) {
        res.status(500).json({ error: error, success: false })
    }
}