FROM node:14

#Create app directory
WORKDIR /user/src/app

# Remove package-lock file.
RUN rm -rf package-lock.json

# Install app dependencies

COPY package.json ./

RUN npm install

COPY . .

EXPOSE 8080

CMD [ "node", "startup.js" ]