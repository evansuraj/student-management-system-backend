const scheduleModel = require('../../models/schedule')

exports.saveSchedule = async (schedule) => {
   return await scheduleModel.create(schedule)
}

exports.getScheduleDetails = async () => {
   return await scheduleModel.find({ isActive: true })
}

exports.filterBySchduleType = async (filterParams) => {
   return await scheduleModel.find({ ...filterParams })
}

exports.getScheduleDetail = async (scheduleId) => {
   return await scheduleModel.find({ scheduleId: scheduleId })
}

exports.updateSchedule = async (schedule) => {
   const options = { upsert: true };
   const filter = { scheduleId: schedule.scheduleId };
   const updateDoc = {
      $set: { ...schedule },
   };
   return await scheduleModel.updateOne(filter, updateDoc, options)
}

exports.deleteSchedule = async (scheduleId, isRemove = false) => {
   if (isRemove === 'true') {
      return await this.removeRecord(scheduleId)
   } else {
      const schedule = await this.getScheduleDetail(scheduleId)
      if (schedule) {
         schedule.isActive = false
         const currentDoc = schedule._doc
         if (currentDoc) {
            const updatedSchedule = await this.updateSchedule(currentDoc)
            if (updatedSchedule) {
               return true
            } else {
               return false
            }
         }
      }
   }
}

exports.removeRecord = async (scheduleId) => {
   const filter = { scheduleId: scheduleId };
   const result = await scheduleModel.deleteOne(filter)
   if (result) {
      return true
   }
   return false
}