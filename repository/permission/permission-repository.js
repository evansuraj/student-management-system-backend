const permissionModel = require('../../models/permission')

exports.savePermission = async (permission) => {
    return await permissionModel.create(permission)
}

exports.getPermissionDetails = async () => {
    return await permissionModel.find({ isActive: true })
}

exports.isExistsPermission = async (permission) => {
    const query = { $or: [{ permissionCode: permission.permissionCode }, { permissionName: permission.permissionName }] }
    return await permissionModel.exists(query)
}

exports.filterPermission = async (filterParams) => {
    return await permissionModel.find({ ...filterParams })
}

exports.getPermission = async (permissionId) => {
    return await permissionModel.findById({ permissionId: permissionId }).populate('roleId')
}

exports.updatePermission = async (permission) => {
    const options = { upsert: true };
    const filter = { permissionId: permission.permissionId };
    const updateDoc = {
        $set: { ...permission },
    };
    return await permissionModel.updateOne(filter, updateDoc, options)
}

exports.deletePermission = async (permissionId, isRemove = false) => {
    if (isRemove === 'true') {
        return await this.removeRecord(permissionId)
    } else {
        const permission = await this.getPermission(permissionId)
        if (permission) {
            permission.isActive = false
            const currentDoc = permission._doc
            if (currentDoc) {
                const updatedPermission = await this.updatePermission(currentDoc)
                if (updatedPermission) {
                    return true
                } else {
                    return false
                }
            }
        }
    }
}

exports.removeRecord = async (permissionId) => {
    const filter = { permissionId: permissionId };
    const result = await permissionModel.deleteOne(filter)
    if (result) {
        return true
    }
    return false
}