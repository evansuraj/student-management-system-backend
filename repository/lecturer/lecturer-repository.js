const lecturerModel = require('../../models/lecturer')

exports.saveLecturer = async (lecturer) => {
    return await lecturerModel.create(lecturer)
}

exports.getLecturerDetails = async () => {
    return await lecturerModel.find({ isActive: true })
}

exports.getLecturer = async (lecturerId) => {
    return await lecturerModel.findOne({ lecturerId: lecturerId })
}

exports.isExists = async (filterParams) => {
    const query = { $or: [{ email: filterParams.email }, { nic: filterParams.nic }] }
    return await lecturerModel.exists(query)
}

exports.updateLecturer = async (lecturer) => {
    const options = { upsert: true };
    const filter = { lecturerId: lecturer.lecturerId };
    const updateDoc = {
        $set: { ...lecturer },
    };
    return await lecturerModel.updateOne(filter, updateDoc, options)
}

exports.deleteLecturer = async (lecturerId) => {
    const lecturer = await this.getLecturer(lecturerId)
    lecturer.isActive = false
    const currentDoc = lecturer._doc
    if (lecturer) {
        const udpatedLecturer = await this.updateLecturer(currentDoc)
        if (udpatedLecturer) {
            return true
        } else {
            return false
        }
    }
}