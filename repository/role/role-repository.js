const roleModel = require('../../models/role')

exports.saveRole = async (role) => {
    return await roleModel.create(role)
}

exports.getRoleDetails = async () => {
    return await roleModel.find({ isActive: true })
}

exports.isExistsRole = async (role) => {
    const query = { $or: [{ roleCode: role.roleCode }, { roleName: role.roleName }] }
    return await roleModel.exists(query)
}

exports.filterRole = async (filterParams) => {
    return await roleModel.find({ ...filterParams })
}

exports.getRole = async (roleId) => {
    return await roleModel.findOne({ roleId: roleId }).populate('permissions')
}

exports.updateRole = async (role) => {
    const options = { upsert: true };
    const filter = { roleId: role.roleId };
    const updateDoc = {
        $set: { ...role },
    };
    return await roleModel.updateOne(filter, updateDoc, options)
}

exports.deleteRole = async (roleId, isRemove = false) => {
    if (isRemove === 'true') {
        return await this.removeRecord(roleId)
    } else {
        const role = await this.getRole(roleId)
        if (role) {
            role.isActive = false
            const currentDoc = role._doc
            if (currentDoc) {
                const updatedrole = await this.updateRole(currentDoc)
                if (updatedrole) {
                    return true
                } else {
                    return false
                }
            }
        }
    }
}

exports.removeRecord = async (roleId) => {
    const filter = { roleId: roleId };
    const result = await roleModel.deleteOne(filter)
    if (result) {
        return true
    }
    return false
}