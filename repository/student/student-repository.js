const studentModel = require('../../models/student')

exports.saveStudent = async (student) => {
   const savedStudent = await studentModel.create(student)
   return savedStudent;
}

exports.getStudentDetails = async () => {
   return await studentModel.find({ isActive: true })
}

exports.isExists = async (filterParams) => {
   const query = { $or: [{ email: filterParams.email }, { nic: filterParams.nic }] }
   return await studentModel.exists(query)
}

exports.getStudent = async (studentId) => {
   return await studentModel.findOne({ studentId: studentId })
}

exports.updateStudent = async (student) => {
   const options = { upsert: true };
   const filter = { studentId: student.studentId };
   const updateDoc = {
      $set: { ...student },
   };
   return await studentModel.updateOne(filter, updateDoc, options)
}

exports.deleteStudent = async (studnetId) => {
   const student = await this.getStudent(studnetId)
   student.isActive = false
   const currentDoc = student._doc
   if (currentDoc) {
      const udpatedStudent = await this.updateStudent(currentDoc)
      if (udpatedStudent) {
         return true
      } else {
         return false
      }
   }
}