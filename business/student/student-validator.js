exports.studentValidator = (student) => {
    const filterParam = {}

    if (student.email) {
        filterParam['email'] = student.email
    }

    if (student.nic) {
        filterParam['nic'] = student.nic
    }

    return { ...filterParam }
}