const passwordGenerator = require('generate-password');
const { v4: uuidv4 } = require('uuid');
const emailService = require('../../external-services/email-service');
const studentRepository = require('../../repository/student/student-repository');
const roleManager = require('../role/role-manager');
const userManager = require('../user/user-manager');
const studentValidator = require('./student-validator')


exports.saveStudent = async (student) => {
    try {
        const filterParam = studentValidator.studentValidator(student)
        const isExistsStudent = await studentRepository.isExists({ ...filterParam })
        if (isExistsStudent) {
            return {
                validity: false,
                error: 'Student is already exists'
            }
        } else {
            const savedStudent = await studentRepository.saveStudent(student);
            console.log("Student created successfully..");
            const roleResult = await roleManager.filterRole({ roleCode: 'STUDENT' });
            const role = roleResult.result.length > 0 ? roleResult.result[0] : null;
            console.log("Get student related roles..");
            if (role) {
                const password = passwordGenerator.generate({ length: 10, numbers: true });
                const user = {
                    userId: uuidv4(),
                    userName: savedStudent.email,
                    firstName: savedStudent.studentFirstName,
                    lastName: savedStudent.studentLastName,
                    middleName: savedStudent.studentMiddleName,
                    userEmail: savedStudent.email,
                    password: password,
                    nic: savedStudent.nic,
                    passportId: savedStudent.passportId,
                    roles: [role._id],
                    permission: role.permissions,
                    isActive: true
                }

                if (user) {
                    const userResult = await userManager.saveUser(user);
                    console.log("User successfully saved..");
                    if (userResult.validity) {
                        const notifiedResult = await emailService.sendEmail(user, password);
                        if (notifiedResult) {
                            return {
                                validity: true,
                                message: "Student created successfully"
                            }
                        }
                    } else {
                        return {
                            validity: false,
                            message: userResult.message
                        }
                    }

                } else {
                    return {
                        validity: false,
                        message: 'Failed to create user'
                    }
                }
            }
        }
    } catch (error) {
        throw error
    }
}

exports.getStudents = async () => {
    try {
        const studentDetails = await studentRepository.getStudentDetails()
        if (studentDetails) {
            return {
                validity: true,
                result: studentDetails
            }
        } else {
            return {
                validity: false,
                result: null
            }
        }
    } catch (error) {
        throw error
    }
}

exports.getStudent = async (studentId) => {
    try {
        const student = await studentRepository.getStudent(studentId)
        if (student) {
            return {
                validity: true,
                result: student
            }
        } else {
            return {
                validity: false,
                result: null
            }
        }
    } catch (error) {
        throw error
    }
}

exports.updateStudent = async (student) => {
    try {
        const updatedStudent = await studentRepository.updateStudent(student)
        if (updatedStudent) {
            return {
                validity: true,
                result: updatedStudent
            }
        } else {
            return {
                validity: false,
                result: null
            }
        }
    } catch (error) {
        throw error
    }
}

exports.deleteStudent = async (studentId) => {
    try {
        const deleted = await studentRepository.deleteStudent(studentId)
        if (deleted) {
            return {
                validity: true,
                result: deleted
            }
        } else {
            return {
                validity: false,
                result: deleted
            }
        }
    } catch (error) {
        throw error
    }
}