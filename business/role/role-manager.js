const roleRepository = require('../../repository/role/role-repository')

exports.saveRole = async (role) => {
    try {
        const isExistsRole = await roleRepository.isExistsRole(role)
        if (isExistsRole) {
            return {
                validity: false,
                result: 'Role is already exists.'
            }
        } else {
            const savedResult = await roleRepository.saveRole(role)
            if (savedResult) {
                return {
                    validity: true,
                    result: savedResult
                }
            } else {
                return {
                    validity: false,
                    result: 'Failed to save role.'
                }
            }
        }
    } catch (error) {
        throw error
    }
}

exports.getRoleDetails = async () => {
    try {
        const roleDetails = await roleRepository.getRoleDetails()
        return {
            validity: true,
            result: roleDetails
        }
    } catch (error) {
        throw error
    }
}

exports.filterRole = async (filterParams) => {
    try {
        const filteredRoles = await roleRepository.filterRole({ ...filterParams })
        return {
            validity: true,
            result: filteredRoles
        }
    } catch (error) {
        throw error
    }
}

exports.getRole = async (roleId) => {
    try {
        const role = await roleRepository.getRole(roleId)
        return {
            validity: true,
            result: role
        }
    } catch (error) {
        throw error
    }
}

exports.updateRole = async (role) => {
    try {
        const updatedRole = await roleRepository.updateRole(role)
        return {
            validity: true,
            result: updatedRole
        }
    } catch (error) {
        throw error
    }
}

exports.deleteRole = async (roleId, isRemove = false) => {
    try {
        const deleted = await roleRepository.deleteRole(roleId, isRemove)
        return {
            validity: true,
            result: deleted
        }
    } catch (error) {
        throw error
    }
}