const scheduleRepository = require('../../repository/schedule/schedule-repository');
const moment = require('moment');

exports.viewAllSchedules = async () => {
    try {
        return await scheduleRepository.getScheduleDetails()
    } catch (error) {
        throw error;
    }
}

exports.saveSchedule = async (schedule) => {
    return await scheduleRepository.saveSchedule(schedule)
}

exports.getScheduleDetails = async () => {
    const schedules = await scheduleRepository.getScheduleDetails()
    let response = [];
    schedules.forEach(res => {


        const dateObj = new Date(res.scheduledDate);
        const dateStr = dateObj.toISOString()

        const stDate = new Date(res.scheduledStartTime);
        const startDateTime = moment(dateStr).set({ "hour": stDate.getHours(), "minute": stDate.getMinutes() });

        const endDate = new Date(res.scheduledEndTime);
        const endDateTime = moment(dateStr).set({ "hour": endDate.getHours(), "minute": endDate.getMinutes() });

        let structuredResult = {
            title: res.selectedCourse,
            subtitle: `Module Name - ${res.scheduledModule} \n Lecturer : ${res.scheduledAttendingLecturer} \n Location : ${res.scheduledLocation} \n\n`,
            start: moment(startDateTime).format("YYYY/MM/DD, h:mm A"),
            end: moment(endDateTime).format("YYYY/MM/DD, h:mm A"),
            id: res.scheduleId,
            color: '#FFF'
        }
        response.push(structuredResult)
    });
    return response;
}

exports.filterBySchduleType = async (filterParams) => {
    return await scheduleRepository.filterBySchduleType(filterParams)
}

exports.getScheduleDetail = async (scheduleId) => {
    return await scheduleRepository.getScheduleDetail(scheduleId)
}

exports.updateSchedule = async (schedule) => {
    return await scheduleRepository.updateSchedule(schedule);
}

exports.deleteSchedule = async (scheduleId, isRemove = false) => {
    return await scheduleRepository.deleteSchedule(scheduleId, isRemove)
}