const lecturerRepository = require('../../repository/lecturer/lecturer-repository')
const lecturerValidator = require('./lecturer-validator')

exports.saveLecturer = async (lecturer) => {
    try {
        const filterParams = lecturerValidator.lectureValidator(lecturer)
        const isExists = await lecturerRepository.isExists({ ...filterParams });
        if (isExists) {
            return {
                validity: false,
                error: 'lecturer is already exists.',
                result: null
            }
        } else {
            const savedRecord = await lecturerRepository.saveLecturer(lecturer)
            return {
                validity: true,
                result: savedRecord
            }
        }
    } catch (error) {
        throw error
    }
}

exports.getLecturerDetails = async () => {
    return await lecturerRepository.getLecturerDetails()
}

exports.getLecturer = async (lecturerId) => {
    return await lecturerRepository.getLecturer(lecturerId)
}

exports.updateLecturer = async (lecturer) => {
    return await lecturerRepository.updateLecturer(lecturer)
}

exports.deleteLecturer = async (lecturerId) => {
    return await lecturerRepository.deleteLecturer(lecturerId)
}