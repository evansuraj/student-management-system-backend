exports.lectureValidator = (lecturer) => {
    const filterParam = {}

    if (lecturer.email) {
        filterParam['email'] = lecturer.email
    }

    if (lecturer.nic) {
        filterParam['nic'] = lecturer.nic
    }

    return { ...filterParam }
}