const app = require('./app')
const mongoose = require('mongoose')
require('dotenv/config')

const environmentVariables = process.env

if (environmentVariables) {
    //database connectivity
    const dbConnection = `mongodb+srv://${environmentVariables.databaseUser}:${environmentVariables.databasePassword}@cluster0.8nvmo.mongodb.net/${environmentVariables.databaseName}?retryWrites=true&w=majority`
    mongoose.connect(`${dbConnection}`, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        dbName: environmentVariables.databaseName
    }).then(() => {
        console.log(`${environmentVariables.dbConnectionGreeting}`);
    }).catch(e => {
        console.log(e);
    })

    let server = app.listen(process.env.PORT || environmentVariables.hostListenerPort, function () {
        let port = server.address().port;
        console.log(`${environmentVariables.applicationServerUpGreeting} ${port}`);
    })
} else {
    console.log('Failed to load application.....');
}
