const express = require('express')
require('dotenv/config')
const router = express.Router()
const lecturerController = require('../controllers/lecturer-controller/lecturer-controller')

const environmentConfigs = process.env

router.get(`${environmentConfigs.lecturerDetailsAction}`, lecturerController.getLecturerDetails)
router.get(`${environmentConfigs.lecturerDetailAction}`, lecturerController.getLecturer)
router.post(`${environmentConfigs.lecturerCreateAction}`, lecturerController.validate('saveLecturer'), lecturerController.saveLecturer)
router.put(`${environmentConfigs.lecturerUpdateAction}`, lecturerController.validate('updateLecturer'), lecturerController.updateLecturer)
router.delete(`${environmentConfigs.lecturerDeleteAction}`, lecturerController.deleteLecturer)

module.exports = router;