const express = require('express')
const router = express.Router()
require('dotenv/config')
const scheduleController = require('../controllers/schedule-controller/schedule-controller')

const environmentConfig = process.env

router.get(`${environmentConfig.viewAllSchedulesAction}`, scheduleController.viewSchedules)
router.get(`${environmentConfig.scheduleDetailsAction}`, scheduleController.getAllSchedule)
router.get(`${environmentConfig.scheduleDetailAction}`, scheduleController.getSchduleDetail)
router.post(`${environmentConfig.scheduleCreateAction}`, scheduleController.validate('saveSchedule'), scheduleController.saveSchedule)
router.post(`${environmentConfig.scheduleFilterationAction}`, scheduleController.getFilteredSchedule)
router.put(`${environmentConfig.scheduleUpdateAction}`, scheduleController.validate('updateSchedule'), scheduleController.updateSchedule)
router.delete(`${environmentConfig.scheduleDeleteAction}`, scheduleController.deleteSchedule)


module.exports = router;