const express = require('express')
require('dotenv/config')
const router = express.Router()
const studentController = require('../controllers/student-controller/student-controller')

const environmentConfigs = process.env

router.get(`${environmentConfigs.studentAllDetailsAction}`, studentController.getStudents)
router.get(`${environmentConfigs.studentDetailAction}`, studentController.getStudent)
router.post(`${environmentConfigs.studentCreateAction}`, studentController.validate('saveStudent'), studentController.saveStudent)
router.put(`${environmentConfigs.studentUpdateAction}`, studentController.validate('updateStudent'), studentController.updateStudent)
router.delete(`${environmentConfigs.deleteStudentAction}`, studentController.deleteStudent)

module.exports = router;