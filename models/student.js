const mongoose = require('mongoose')

// This schema will handle all the details of the student.
// TODO : Later add more fields.
const studentSchema = mongoose.Schema({
    studentId: { type: String },
    studentFirstName: { type: String },
    studentLastName: { type: String },
    studentMiddleName: { type: String },
    email: { type: String },
    studentOtherEmail: { type: String },
    gender: { type: String },
    dob: { type: String },
    age: { type: Number },
    batchYear: { type: String },
    batch: { type: String },
    studentSystemNumber: { type: String },
    nic: { type: String },
    passsportId: { type: String },
    profileImage: { type: String },
    studentAttachments: { type: String },
    isActive: Boolean
})

const Student = mongoose.model('Student', studentSchema)
module.exports = Student;