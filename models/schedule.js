const mongoose = require('mongoose')

// This schema will handle all the schedule inside the student management. | EXAM-SCHEDULE | LECTURE-SCHEDULE
const lecturerScheduleSchema = mongoose.Schema({
    scheduleId: { type: String },
    selectedCourse: { type: String },
    batch: { type: String },
    scheduleType: { type: String },
    scheduledDate: { type: String },
    scheduledStartTime: { type: String },
    scheduledEndTime: { type: String },
    scheduledLocation: { type: String },
    scheduledModule: { type: String },
    scheduledAttendingLecturer: { type: String },
    isActive: { type: Boolean },
})

const Schedule = mongoose.model('Schedule', lecturerScheduleSchema)
module.exports = Schedule;