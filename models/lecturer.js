const mongoose = require('mongoose')

// This schema will handle the all the details of the lecturer. 
// TODO : add more fields in later phases.
const lecturerSchema = mongoose.Schema({
    lecturerId: { type: String },
    lecturerFirstName: { type: String, default: '' },
    lecturerLastName: { type: String, default: '' },
    lecturerMiddleName: { type: String, default: '' },
    email: { type: String, default: '' },
    lecturerOtherEmail: { type: String, default: '' },
    nic: { type: String, default: '' },
    passsportId: { type: String, default: '' },
    profileImage: { type: String, default: '' },
    lecturerAttachments: [{ type: String }],
    isActive: { type: Boolean, default: true }
})

const Lecturer = mongoose.model('Lecturer', lecturerSchema)
module.exports = Lecturer;